# README #

Install modules:

"npm install express"
"npm install nodemon"

Start server:
"nodemon index.js" command at project root directory.

API samples:

Call of function "sum" example:
http://localhost:3333/sum?array=[1,2,4]

Call of function "average" example:
http://localhost:3333/average?array=[2,4]
