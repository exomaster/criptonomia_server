const numbers = require('../lib/Numbers');

module.exports = {
  async sum(request, response) {
    var array_int = JSON.parse(request.query.array);
    const sum = await numbers.sum(array_int);
    return response.json({sum});
  },
  async avg(request, response) {
    var array_int = JSON.parse(request.query.array);
    const avg = await numbers.avg(array_int);
    return response.json({avg});
  }
};
