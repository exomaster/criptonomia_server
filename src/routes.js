const express = require('express');
const NumberController = require('./controllers/NumberController');
const routes = express.Router();

routes.get('/sum', NumberController.sum);
routes.get('/average', NumberController.avg);

module.exports = routes;
