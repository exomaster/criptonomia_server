const number = require('./Number');

module.exports = {
  sum(array_int) {
    var acc = 0;
    for ( var i = 0; i < array_int.length; i++ ){
      acc = number.sum(parseInt(array_int[i]),acc);
    }
    return acc;
  },
  avg(array_int) {
    var sum = module.exports.sum(array_int);
    return number.divide(sum,array_int.length)
  }
}
